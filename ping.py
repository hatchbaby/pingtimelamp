# This file essentially does this:
# ping time.hatchbaby.com | while read line; do echo `date` - $line; done > time.txt
# ping <lampIP> | while read line; do echo `date` - $line; done > lamp.txt

import subprocess
import sys
import os
from zipfile import ZipFile 
import signal
import time

# The Lamp's IP. This will be overwritten by user's command line input
lampURL = '10.0.0.80' 
lampFile = 'lamp.txt'

# The time server
timeURL = 'time.hatchbaby.com'
timeFile = 'time.txt'
zipFile = 'logs.zip'

def printUsage():
    print('Usage: python ping.py <lampIP>')

def processArgs():
    if len(sys.argv) < 2:
        printUsage()
        exit(1)
    else:
        global lampURL
        lampURL = sys.argv[1]
        deleteFileIfExists(lampFile)
        deleteFileIfExists(timeFile)
        deleteFileIfExists(zipFile)
        ping()

def deleteFileIfExists(fileName):
    if os.path.exists(fileName):
        os.remove(fileName)
        print('Deleted ' + fileName)

def ping():    
    timeProcess = subprocess.Popen(['ping', timeURL], 
                            stdout=subprocess.PIPE,
                            universal_newlines=True)

    lampProcess = subprocess.Popen(['ping', lampURL], 
                            stdout=subprocess.PIPE,
                            universal_newlines=True)


    global timeFile
    global lampFile
    while True:
        # Get date in string form
        date = subprocess.Popen(['date'], stdout=subprocess.PIPE).stdout.read().strip() + ' - '
        
        # Get outout from time ping and output to console and file
        timeOutput = date + timeProcess.stdout.readline().strip()
        print(timeOutput)
        with open(timeFile, "a") as timeFileRef:
            timeFileRef.write(timeOutput + '\n')

        # Get outout from lamp ping and output to console and file
        lampOutput = date + lampProcess.stdout.readline().strip()
        print(lampOutput)
        with open(lampFile, 'a') as lampFileRef:
            lampFileRef.write(lampOutput + '\n')

def zipFiles():
    global timeFile
    global lampFile
    global zipFile

    # Compress our txt files into a zip
    with ZipFile(zipFile,'w') as zipFileRef: 
        zipFileRef.write(timeFile) 
        zipFileRef.write(lampFile) 

def exit_gracefully(signum, frame):
    # User pressed ctrl+c
    # zip up files and exit gracefully
    zipFiles()
    sys.exit(1)

if __name__ == '__main__':
    # Observe ctrl+c quit signal
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)

    # Process our args
    processArgs()




