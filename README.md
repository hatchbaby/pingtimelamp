Generates two ping logs. 
  
1.) Your Rest+ IP address (lamp.txt)

2.) time.hatchbaby.com (time.txt)

Press ctrl+c to stop. Both txt files will then be zipped (logs.zip). You will find your txt/zip files in the same directory. 

txt/zip files are purged on script start. 


````
# Usage: python ping.py <lampIP>
python ping.py 10.0.0.80
````

